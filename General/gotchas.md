# Gotchas
*For when you're stuck*

## `ldapsearch` Usage
- Updates to the software have made many tutorials out of date.  The `ldapsearch-h target_ip` syntax is old and busted.  The new hotness is `ldapsearch -H ldap://target_ip`

## `impacket` Weirdness
- I hate and love using `impacket`.  It's a super useful tool, but it is *not* a streamlined product.  I have multiple versions installed and the newer one does not work on the Hack the Box "Search" machine where you have to make use of `GetUserSPNs.py`.  The old one did work.
