# Hacking Help
These are my compiled Hacking Help resources derived from multiple OSCP attempts and various bits and bobs yoinked from around the internet.  Credit is given where credit is known.

## General Help
- [PowerShell](powershell.md)
- [General Links](links.md)
- [Active Directory Cheat Sheet](ad_cheatsheet.md)
- [Active Directory Links](ad_links.md)
- [Cheatsheets for Tools](General/cheatsheet_links.md)
- [Gotchas](General/gotchas.md)

## Reconnaissance
### Network Recon
- [Network Reconnaissance](Recon/network_recon.md): Nmap and autorecon use
### Active Directory Recon
### SMB Recon 
- [SMB Recon and Attacks](Recon/smb_recon_attacks.md)

## Vulnerabilities and Exploitation
### Guides
#### Web Apps
- [Classic XXE template](classic-xxe.xml)
- NEED: Common injections
### Tools
- .

## Post Exploitation
### Guides
- [Post Exploitation](Post-Exploitation/post-exploitation.md)
- [Windows Lateral Movement](Post-Exploitation/windows-lateral-movement.md)
- [Moving Files Around](Post-Exploitation/moving_files.md)
- [Pillaging Active Directory](Post-Exploitation/pillaging_ad.md)
- [Hunting for Credentials](Post-Exploitation/hunting_creds.md)
- NEED: Mimikatz
### Tools
- [Linux Exploit Suggester](https://github.com/mzet-/linux-exploit-suggester)
- [Windows Exploit Suggester](https://github.com/AonCyberLabs/Windows-Exploit-Suggester)

### Privilege Escalation
#### Guides
- [Windows Privilege Escalation](Post-Exploitation/windows_privesc.md)
- [Golden Ticket Attack](Post-Exploitation/golden-ticket.md)
- [DLL Hijacking](Post-Exploitation/dllhijacking.md)
#### Tools
- [WinPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/winPEAS)
- [LinPEAS](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS)
- [PowerSploit](https://github.com/PowerShellMafia/PowerSploit/tree/master/Privesc)

## Web Application Penetration Testing
- [Web Apps Engagements](Web-Apps/webapps.md)
- [Tool Matrix](Web-Apps/webapp-tools.md)
