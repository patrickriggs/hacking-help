# Web Application Penetration Testing Reconnaissance Tools

Credit: Data originally derived from a [linkedin graphic](https://media-exp1.licdn.com/dms/image/C4D22AQGpE0qmqM4GiQ/feedshare-shrink_800/0/1650333489284?e=2147483647&v=beta&t=zUET74VC3Gkx9WjyMiKft2g4FpFwvuyb7UNcdXyrrRU) credited to [@_cyberkhalid](https://twitter.com/_cyberkhalid)

| Task | Resource Name | RTFM | Comments |
|---|---|---|---|
|Proxy|Burp Suite / Burp Suite Pro | | BSP is faster and has an automated scanner |
|Proxy|OWASP Zap Proxy | | Better fuzzer than Burp Suite non-Pro |
|Subdomains|subfinder| | |
|Subdomains|amass| | |
|Subdomains|dig| | |
|Subdomains|assetfinder| | |
|Subdomains|sublist3r| | From his Practical Ethical Hacking course, (IIRC) Heath 'The Cyber Mentor' Adams says he's moved off of using this in favor of amass|
|Subdomains|chaos|[Website](https://chaos.projectdiscovery.io)|Pre-reconned subdomain data for companies or websites participating in bug bounty programs|
|Spidering|gospider|[GitHub](https://github.com/jaeles-project/gospider)||
|Spidering|gau|||
|Spidering|linkfinder|||
|Spidering|waybackurls|||
|Spidering|hakrawler|||
|Spidering|paramspider|||
|Content Discovery|ffuf|[GitHub](https://github.com/ffuf/ffuf)||
|Content Discovery|wfuzz|[Read The Docs](https://wfuzz.readthedocs.io/en/latest/)||
|Content Discovery|dirbuster|[Kali Description](https://www.kali.org/tools/dirbuster/)|By all accounts (mostly OWASP burying the project on their sites), dirbuster seems to be on the outs.  I've been using feroxbuster mostly, anyway.|
|Content Discovery|gobuster|[GitHub](https://github.com/OJ/gobuster)|This was the tool most used during my OffSec PWK Academy course.|
|Content Discovery|feroxbuster|[GitHub](https://github.com/epi052/feroxbuster)|AKA Ferric Oxide Buster.  My current go-to.|
|Fingerprinting|wappalyzer|[GitHub](https://github.com/wappalyzer/wappalyzer)|I've only ever used the browser extensions but apparently there is a command line option.|
|Fingerprinting|Built With|[Website](https://builtwith.com)|Will only work with a publicly available website (i.e. not on intranets) but is very thorough about a site's infrastructure.|
|Fingerprinting|netcraft|[Website](https://www.netcraft.com/internet-data-mining/)|Scroll down to the "Internet Research Tools" section, almost at the bottom.|
|Fingerprinting|whatweb|[GitHub](https://github.com/urbanadventurer/WhatWeb)|Command line tool used to ID a site's tech or stack.|
|Fingerprinting|wafw00f|[GitHub](https://github.com/EnableSecurity/wafw00f)|Identify and fingerprint web application firewalls.|

## Vulnerability
1. nuclei
2. wpscan
3. nikto

## E-Mail
1. mxtoolbox
2. emkei
3. anonymailer
4. thunderbird

## Exploitation
1. searchsploit
2. exploitdb

## Sensitive Data
1. Trufflehog
2. gitsecrets

## API
1. postman
2. graphqlmap

## Payloads and Wordlists
1. swisskeyrepo
2. seclists

## Ports
1. nmap
2. masscan
3. zmap
4. smap
5. hackertarget

## SSL
1. sslscan
2. sslhopper

## Search Engines
1. shodan
2. censys
3. zoomeye
4. google

## Miscellaneous or Multiple Coverage
1. httpx
2. metasploit
3. dnsdumpster
4. httprobe
5. recon-ng
6. securitytrails
