# Web Application Pen Testing

*Sources* 
* TryHackMe: *https://tryhackme.com/room/rpburpsuite*
* PWAST by Taggart: *https://academy.tcm-sec.com/courses/1792341/lectures/40496408*

## Mission Resources
* [OWASP Top 10 Checklist PDF](https://owasp.org/www-project-web-security-testing-guide/assets/archive/OWASP_Testing_Guide_v4.pdf)
* [Testing Checklist Spreadsheet](https://github.com/tanprathan/OWASP-Testing-Checklist)

## Web Development
* [HTMLReference.io](https://htmlreference.io)
* [w3schools Tag Reference](https://w3schools.com/tags/default.asp)

## Web Assessment Parameters
### Data Received Prior to Assessment
* The application URL (hopefully for dev/test and not prod)
* A list of the different user roles within the application
* Various test accounts and associated credentials for those accounts
* A list of pieces/forms in the application which are out-of-scope for testing and should be avoided

## Burp Suite
### Components
* **Proxy** - What allows us to funnel traffic through Burp Suite for further analysis
* **Target** - How we set the scope of our project. We can also use this to effectively create a site map of the application we are testing.
* **Intruder** - Incredibly powerful tool for everything from field fuzzing to credential stuffing and more
* **Repeater** - Allows us to 'repeat' requests that have previously been made with or without modification. Often used in a precursor step to fuzzing with the aforementioned Intruder
* **Sequencer** - Analyzes the 'randomness' present in parts of the web app which are intended to be unpredictable. This is commonly used for testing session cookies
* **Decoder** - As the name suggests, Decoder is a tool that allows us to perform various transforms on pieces of data. These transforms vary from decoding/encoding to various bases or URL encoding.
* **Comparer** - Comparer as you might have guessed is a tool we can use to compare different responses or other pieces of data such as site maps or proxy histories (awesome for access control issue testing). This is very similar to the Linux tool diff.
* **Extender** - Similar to adding mods to a game like Minecraft, Extender allows us to add components such as tool integrations, additional scan definitions, and more!
* **Scanner** - Automated web vulnerability scanner that can highlight areas of the application for further manual investigation or possible exploitation with another section of Burp. This feature, while not in the community edition of Burp Suite, is still a key facet of performing a web application test.

### Intruder Attack Types
1. **Sniper** - The most popular attack type, this cycles through our selected positions, putting the next available payload (item from our wordlist) in each position in turn. This uses only one set of payloads (one wordlist).

2. **Battering Ram** - Similar to Sniper, Battering Ram uses only one set of payloads. Unlike Sniper, Battering Ram puts every payload into every selected position. Think about how a battering ram makes contact across a large surface with a single surface, hence the name battering ram for this attack type.

3. **Pitchfork** - The Pitchfork attack type allows us to use multiple payload sets (one per position selected) and iterate through both payload sets simultaneously. For example, if we selected two positions (say a username field and a password field), we can provide a username and password payload list. Intruder will then cycle through the combinations of usernames and passwords, resulting in a total number of combinations equalling the smallest payload set provided. 

4. **Cluster Bomb** - The Cluster Bomb attack type allows us to use multiple payload sets (one per position selected) and iterate through all combinations of the payload lists we provide. For example, if we selected two positions (say a username field and a password field), we can provide a username and password payload list. Intruder will then cycle through the combinations of usernames and passwords, resulting in a total number of combinations equalling usernames x passwords. Do note, this can get pretty lengthy if you are using the community edition of Burp. 

## SQL Injection Vulnerabilities
* SQL syntax reminders
    * `'` and `"` are string delimiters
    * `--`, `/*`, and `#` are comment delimiters
    * `*` and `%` are wildcards
    * `;` ends an SQL statement
    * `= + > < ()` generally follow programming logic
* [SQL Injection wordlist](https://github.com/fuzzdb-project/fuzzdb/blob/master/attack/sql-injection/detect/xplatform.txt)

## Analyzing Randomness
Burp Suite's Sequencer analyzes quality of randomness and other things intended to be unpredictable.  The most commonly analyzed items include:
* Session tokens
* Anti-CSRF (Cross-Site Request Forgery) tokens
* Password reset tokens (sent with password resets that in theory uniquely tie users with their password reset requests)
[PortSwigger Documentation]()

## Reconnaissance
* [Directory Brute Forcing with Feroxbuster](https://www.geeksforgeeks.org/feroxbuster-recursive-content-discovery-tool-in-kali-linux/)

## Cross-Site Scripting (XSS)
* [DOM Based XSS](https://www.scip.ch/en/?labs.20171214)
* [PortSwigger Cross Site Scripting Cheatsheet](https://portswigger.net/web-security/cross-site-scripting/cheat-sheet)
* [GitHub pgaijin66 XSS Payloads](https://github.com/pgaijin66/XSS-Payloads/blob/master/payload/payload.txt)
* [XSS Practice from Google](http://xss-game.appspot.com)
    * [Talked-out solutions to those Games](http://offsec-sureshatt.blogspot.com/2017/04/solving-all-google-xss-game-levels.html)

## XML External Entity Attacks (XXE)
* [XXE Injection Payloads](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XXE%20Injection)
