# Active Directory Resources
*Started with links from TCM's Practical Ethical Hacking course notes.*

## AD Links In General
* [Active Directory Methodology](https://book.hacktricks.xyz/windows/active-directory-methodology)
* [Active Directory Security Blog](https://adsecurity.org/)
* [Harmj0y Blog](http://www.harmj0y.net/blog/blog/)
* [Pentester Academy Active Directory](https://www.pentesteracademy.com/activedirectorylab)
* [eLearnSecurity eCPTXv2](https://elearnsecurity.com/product/ecptx-certification/)
* [S1ckB0y1337 AD Exploitation Cheat Sheet](https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet)

## Initial Attack Vectors
* [Top Five Ways I Got Domain Admin On Your Internal Network Before Lunch](https://medium.com/@adam.toscher/top-five-ways-i-got-domain-admin-on-your-internal-network-before-lunch-2018-edition-82259ab73aaaQ)
* [IPv6 DNS Takeover With mitm6](https://blog.fox-it.com/2018/01/11/mitm6-compromising-ipv4-networks-via-ipv6/)
* [Combining NTLM Relays and Kerberos Delegation](https://dirkjanm.io/worst-of-both-worlds-ntlm-relaying-and-kerberos-delegation/)
* [A Pen Tester's Guide to Printer Hacking](https://www.mindpointgroup.com/blog/how-to-hack-through-a-pass-back-attack/)

## Post Compromise Enumeration
* [PowerView Cheat Sheet](https://gist.github.com/HarmJ0y/184f9822b195c52dd50c379ed3117993)

## Post Compromise Attacks
* [Impersonate User Tokens](https://www.offensive-security.com/metasploit-unleashed/fun-incognito/)
* [Kerberoasting](https://medium.com/@Shorty420/kerberoasting-9108477279cc)
* [Group Policy Pwnage](https://blog.rapid7.com/2016/07/27/pentesting-in-the-real-world-group-policy-pwnage/)
* [Active Directory Attacks](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md)
* [SCF and URL file attack against writable share](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md#scf-and-url-file-attack-against-writeable-share)
* [PrintNightmare: cube0x0 RCE](https://github.com/cube0x0/CVE-2021-1675)
* [PrintNightmare: calebstewart LPE](https://github.com/calebstewart/CVE-2021-1675)
* [mimikatz](https://github.com/gentilkiwi/mimikatz)

## Miscellaneous Information
* Impacket versions > 0.9.19 are unstable and causing issues for students and pentesters alike. Try purging impacket completely and downloading 0.9.19 from here: (https://github.com/SecureAuthCorp/impacket/releases)
