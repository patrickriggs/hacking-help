# Industrial Control Systems
## General Information

## Links
* Rob Lee, author of the SANS ICS pipeline - https://www.robertmlee.org/a-collection-of-resources-for-getting-started-in-icsscada-cybersecurity/
* https://ics-training.inl.gov/learn

## People
* Rob Lee - https://twitter.com/RobertMLee
