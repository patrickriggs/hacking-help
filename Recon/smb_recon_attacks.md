# SMB Reconnaissance and Attacks
> Ports 139 and 445

Source(s): 
- https://github.com/six2dez/OSCP-Human-Guide/blob/master/oscp_human_guide.md#port-139445---smb
- https://oscp.infosecsanyam.in/untitled/smb-enumeration

## Reconnaissance
Do all simple enumeration including userlist, sharelist, group and member list, password policy, RID cycling for users, OS information, NetBIOS lookup, printer information
- `enum4linux -a $ip`

Runs smb-enum-domains, -groups, -processes, -services, -sessions, -shares, -users
- `nmap --script=smb-enum* --script-args=unsafe=1 -T5 $ip`

Get Versions
- `smbclient -L \\\\$ip`

Connect so a SMB share as a guest
- `smbmap -u guest -H $ip`

Connect to a share, run a directory listing, and print only the first column of data
- `smbclient -N \\\\$ip\\share -c ls | awk '{ print $1 }'`

If you have a Kerberos port open (88) and a list of usernames, you can see if any of them have Kerberos pre-authentication disabled and are susceptible to an ASREPRoasting attack.
- `GetNPUsers.py domain.tld/ -no-pass -usersfile users.txt -dc-ip $ip | grep -v 'KDC_ERR_C_PRINCIPAL_UNKNOWN'`

This is more attacking than recon, but it follows right after you get results from the GetNPUsers.py attack.  Using John The Ripper to crack the hash.
- `john hash --format=krb5asrep`

If you have a username and password, you can try to use crackmapexec to enumerate the smb file shares permissions
- `crackmapexec smb $ip -u username -p 'password' --shares`