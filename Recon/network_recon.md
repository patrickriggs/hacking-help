# Network Reconnaissance
> Get a jump start on your opening salvo

## `nmap` Commands
Bespoke, noisy, full throttle TCP interrogation
- `nmap -sS -A -T4 -p- -Pn -oA full $ip`
  - `-sS`: TCP SYN scan
  - `-A`: OS detection (-O), version detection (-sV), script scanning (-sC), and traceroute (--traceroute)
  - `-T4`: 4/5 timing template (80% maximum speed)
  - `-p-`: All TCP ports
  - `-Pn`: Treat all hosts as online (ignore discovery) 
  - `-oA`: Output results in xml, grep, and nmap formats

UDP port scan that, based on port number, presumes the service and sends the appropriate traffic to elicit service version
- `sudo nmap -sU -sV --version-intensity 0 -F -n $ip`
  - `-sU`: UDP port scan
  - `-sV`: Version detection
  - `--version-intensity`: Light touch.  Assume the single most likely service and send a probe based on that
  - `-F`: Fast scan.  Reduces nmap default of 1000 ports to 100.
    - `--top-ports`: Alternatively, specify the top ### used UDP ports
  - `-n`: No DNS resolution

## `autorecon.py` Use
Automated blast of packets and logical follow-on spinoff activity
- `sudo which $(autorecon) --dirbuster.tool feroxbuster $ip`

