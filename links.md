# Useful Links

## OSCP Execution
* Tmux Cheat Sheets
  * https://tmuxcheatsheet.com/
  * https://gist.github.com/MohamedAlaa/2961058
* Tib3rius Pentest-Cheatsheets - https://github.com/Tib3rius/Pentest-Cheatsheets
* Awesome OSCP The Lynx Team Fork - https://github.com/The-Lynx-Team/awesome-oscp
* OSCP Resources from The Lynx Team - https://github.com/The-Lynx-Team/OSCP
* Upgrading Simple Shells to Fully Interactive TTYs (ropnop) - https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/
* John the Ripper Cheat Sheet - https://countuponsecurity.files.wordpress.com/2016/09/jtr-cheat-sheet.pdf
* hashcat Example Hashes - https://hashcat.net/wiki/doku.php?id=example_hashes
* Julia Evans (@b0rk) is a motherfucking saint - https://wizardzines.com/
* OSCP Human Guide - https://github.com/six2dez/OSCP-Human-Guide/blob/master/oscp_human_guide.md#suid-misconfiguration
  * Has multiple techniques and commands written out for various stages of a pen test and targeted to specific ports and services
* Unofficial OSCP Approved Tools - https://falconspy.medium.com/unofficial-oscp-approved-tools-b2b4e889e707
* HackTricks - https://book.hacktricks.xyz/


## Vulnerability Research
*Don't just rely on `searchsploit`, searching Exploit-DB.com can bring back additional results.*
* National Vulnerability Database - https://nvd.nist.gov/vuln/full-listing
* ExploitDB - http://exploit-db.com/
* Almost every publicly available CVE PoC - https://github.com/trickest/cve 

## Reporting
* Reporting Notes & Resources by John Hammond - https://www.youtube.com/watch?v=MQGozZzHUwQ
* Markdown to PDF Report Template - https://github.com/noraj/OSCP-Exam-Report-Template-Markdown
* Getting Images into Markdown Document and Weblog Posts with Markdown Monster - https://medium.com/markdown-monster-blog/getting-images-into-markdown-documents-and-weblog-posts-with-markdown-monster-9ec6f353d8ec
### Note-Taking Templates
* CherryTree
  * https://guide.offsecnewbie.com/cherrytree-oscp-template

## Techniques
### Reverse Shells
* PayloadsAllTheThings - https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md
* Highon.coffee - https://highon.coffee/blog/reverse-shell-cheat-sheet/
* Reverse Shell Generator - https://www.revshells.com/

### Videos
* Sagi Shahar Video Series - https://www.youtube.com/channel/UCcU4a3acOkH43EjNfpprIEw

### Repos
* 740i GitHub - https://github.com/740i/pentest-notes/

### Web
* 740i Web Techniques - https://github.com/740i/pentest-notes/blob/master/web.md
  * SQL injection strings, RFI/LFI, encoding, etc.
* Web Testing on OSCP - https://guif.re/webtesting
* All SQL Injections! - https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/SQL%20Injection

### Pivoting
* 740i Pivoting Techniques - https://github.com/740i/pentest-notes/blob/master/pivoting.md

### Privilege Escalation
* Sagi Shahar Local Windows/Linux Privilege Escalation Workshop - https://github.com/sagishahar/lpeworkshop

#### Windows
* 740i Windows Privesc Notes - https://github.com/740i/pentest-notes/blob/master/windows-privesc.md

#### Linux
* 740i Linux Privesc Notes - https://github.com/740i/pentest-notes/blob/master/linux-privesc.md

## Tools
### PowerShell Empire
* Using PowerShell Empire - https://www.cs.umb.edu/~ckelly/teaching/common/p/security/p02_REMOTE.pdf
* Ultimate Guide to PowerShell Empire - https://hackmag.com/security/powershell-empire/
